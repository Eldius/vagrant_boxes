@echo off

cls

set "HOME_DIR=%CD%"
set "SSH_CMD=%HOME_DIR%\bin\plink.exe"
set "STOP_CMD=sudo service jetty stop"

set "SERVER_PASS=vagrant"
set "SERVER_USER=vagrant"
set "CLEAN_WORK_CMD=sudo rm /vagrant/target/webapps/logs/*"
set "CLEAN_LOGS_CMD=sudo rm -rf /vagrant/target/webapps/work/*"
set "START_CMD=sudo service jetty start"

call %SSH_CMD% -ssh -P 2222 -pw %SERVER_PASS% %SERVER_USER%@localhost %STOP_CMD%

cd %HOME_DIR%\projects\$1
call mvn clean install -DskipTests=true
if NOT "%ERROR_CODE%" == "0" GOTO ERROR

call copy %HOME_DIR%\projects\$2\target\*.war %HOME_DIR%\target\webapps\webapps /Y

call %SSH_CMD% -ssh -P 2222 -pw %SERVER_PASS% %SERVER_USER%@localhost %CLEAN_WORK_CMD%
call %SSH_CMD% -ssh -P 2222 -pw %SERVER_PASS% %SERVER_USER%@localhost %CLEAN_LOGS_CMD%
call %SSH_CMD% -ssh -P 2222 -pw %SERVER_PASS% %SERVER_USER%@localhost %START_CMD%

goto FINISH

:ERROR
echo Error: %ERROR_CODE%

:FINISH
cd %HOME_DIR%
msg * Finished!
