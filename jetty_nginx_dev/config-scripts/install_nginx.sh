#!/bin/bash

export HTTP_ROOT="/vagrant/target/www"

IFS='/' read -a array <<< "$HTTP_ROOT"

export TMP_FOLDER=""
for index in "${!array[@]}"
do
    echo "###############################################"
    if [ "${array[index]}" != "" ];
    then
        echo "folder '$TMP_FOLDER'"
        TMP_FOLDER="$TMP_FOLDER/${array[index]}"
        if [ -f "$TMP_FOLDER" ];
        then
            echo "folder '$TMP_FOLDER' already exists"
        else
            echo "creating folder '$TMP_FOLDER'"
            mkdir $TMP_FOLDER
        fi
    fi
    echo "###############################################"
done

apt-get install nginx -y


#cp /vagrant/config-scripts/config-files/index.html $HTTP_ROOT
cat <<EOT >> $HTTP_ROOT/index.html
<html>
    <head>
        <title>Home</title>
    </head>
    <body>
        <h1>Home</h1>
    </body>
</html>

EOT

#cp /vagrant/config-scripts/config-files/nginx.conf /etc/nginx
rm /etc/nginx/nginx.conf
touch /etc/nginx/nginx.conf

cat <<EOT >> /etc/nginx/nginx.conf

# location: /etc/nginx/nginx.conf

user www-data;
worker_processes 4;
pid /var/run/nginx.pid;

events {
	worker_connections 768;
	# multi_accept on;
}


http {

	server {
		listen 80;
		location / {
			root              /vagrant/target/www;
			access_log        off;
			expires           -1;
			add_header        Cache-Control private;
		}

		location /api/ {
			proxy_pass http://localhost:8080/;
		}

	}

	##
	# Basic Settings
	##

	sendfile off;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
	# server_tokens off;

	# server_names_hash_bucket_size 64;
	# server_name_in_redirect off;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	##
	# Logging Settings
	##

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	##
	# Gzip Settings
	##

	gzip on;
	gzip_disable "msie6";

	# gzip_vary on;
	# gzip_proxied any;
	# gzip_comp_level 6;
	# gzip_buffers 16 8k;
	# gzip_http_version 1.1;
	# gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

	##
	# nginx-naxsi config
	##
	# Uncomment it if you installed nginx-naxsi
	##

	#include /etc/nginx/naxsi_core.rules;

	##
	# nginx-passenger config
	##
	# Uncomment it if you installed nginx-passenger
	##

	#passenger_root /usr;
	#passenger_ruby /usr/bin/ruby;

	##
	# Virtual Host Configs
	##

	include /etc/nginx/conf.d/*.conf;
	# include /etc/nginx/sites-enabled/*;
}


#mail {
#	# See sample authentication script at:
#	# http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
#	# auth_http localhost/auth.php;
#	# pop3_capabilities "TOP" "USER";
#	# imap_capabilities "IMAP4rev1" "UIDPLUS";
#
#	server {
#		listen     localhost:110;
#		protocol   pop3;
#		proxy      on;
#	}
#
#	server {
#		listen     localhost:143;
#		protocol   imap;
#		proxy      on;
#	}
#}

EOT


service nginx restart
