echo ##### INSTALL JETTY #####

export WEBAPPS_HOME=/vagrant/target/webapps

# preparing webapps folder
IFS='/' read -a array <<< "$WEBAPPS_HOME"

export TMP_FOLDER=""
for index in "${!array[@]}"
do
    echo "folder '$TMP_FOLDER'"
    if [ "${array[index]}" != "" ];
    then
        TMP_FOLDER="$TMP_FOLDER/${array[index]}"
        if [ -f "$TMP_FOLDER" ];
        then
            echo "folder '$TMP_FOLDER' already exists"
        else
            echo "creating folder '$TMP_FOLDER'"
            mkdir $TMP_FOLDER
        fi
    fi
done


cd /tmp
wget http://download.eclipse.org/jetty/stable-9/dist/jetty-distribution-9.2.10.v20150310.tar.gz
tar xzvf jetty-distribution-*.gz

mv jetty-distribution-*/ /opt/jetty
rm jetty-distribution-*.tar.gz

rm -rf /opt/jetty/demo-base/

cat <<EOT >> /etc/environment
JAVA_HOME=/usr/lib/jvm/java-7-oracle
JETTY_HOME=/opt/jetty
JETTY_BASE=$WEBAPPS_HOME

EOT

useradd --user-group --shell /bin/false --home-dir /opt/jetty/temp jetty

cd /opt

# creating work folders
temp="$WEBAPPS_HOME/temp"

if [ ! -e $temp ]
  then
    mkdir $temp
fi

temp="$WEBAPPS_HOME/work"
if [ ! -e $temp ]
  then
    mkdir $temp
fi

temp="$WEBAPPS_HOME/logs"
if [ ! -e $temp ]
  then
    mkdir $temp
fi

mkdir jetty/temp/


usermod -a -G jetty jetty

cp /opt/jetty/bin/jetty.sh /etc/init.d/jetty

touch /etc/default/jetty


cat <<EOT >> /etc/default/jetty
JETTY_HOME=/opt/jetty
JETTY_BASE=$WEBAPPS_HOME
TMPDIR=/opt/jetty/temp

EOT

cp /opt/jetty/start.ini $WEBAPPS_HOME

cat <<EOT >> $WEBAPPS_HOME/start.ini

#--module=cdi

#--module=logging

#--module=debug

#--module=monitor

#--module=requestlog

#===========================================================
# Configure JVM arguments.
# If JVM args are include in an ini file then --exec is needed
# to start a new JVM from start.jar with the extra args.
# If you wish to avoid an extra JVM running, place JVM args
# on the normal command line and do not use --exec
#-----------------------------------------------------------
--exec
-Xdebug
-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n
# -Xmx2000m
# -Xmn512m
# -XX:+UseConcMarkSweepGC
# -XX:ParallelCMSThreads=2
# -XX:+CMSClassUnloadingEnabled
# -XX:+UseCMSCompactAtFullCollection
# -XX:CMSInitiatingOccupancyFraction=80
# -verbose:gc
# -XX:+PrintGCDateStamps
# -XX:+PrintGCTimeStamps
# -XX:+PrintGCDetails
# -XX:+PrintTenuringDistribution
# -XX:+PrintCommandLineFlags
# -XX:+DisableExplicitGC

EOT

cd $WEBAPPS_HOME

echo y | java -jar /opt/jetty/start.jar --create-files

chown -R jetty:jetty $WEBAPPS_HOME
chown -R jetty:jetty /opt/jetty

update-rc.d jetty defaults

cd $WEBAPPS_HOME/lib/ext

wget http://central.maven.org/maven2/org/jboss/weld/servlet/weld-servlet/2.2.9.Final/weld-servlet-2.2.9.Final.jar

service jetty start
