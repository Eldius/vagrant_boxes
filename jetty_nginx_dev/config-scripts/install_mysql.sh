#!/bin/sh

apt-get update

cat << EOF | debconf-set-selections
mysql-server-5.0 mysql-server/root_password password 123Senha
mysql-server-5.0 mysql-server/root_password_again password 123Senha
mysql-server-5.0 mysql-server/root_password seen true
mysql-server-5.0 mysql-server/root_password_again seen true
EOF

apt-get install -y mysql-server

sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf

service mysql restart

mysql -u root --password=123Senha -t <<STOP
-- This is a comment inside an sql-command-stream.
create database development;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'
show databases;
\q
STOP
test $? = 0 && echo "Your batch job terminated gracefully"

