#!/bin/bash

# choose ok on install menu
echo debconf shared/accepted-oracle-license-v1-1 select true | \
debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
debconf-set-selections

#install java 7
apt-get update
apt-get install -y python-software-properties
add-apt-repository ppa:webupd8team/java
apt-get update
apt-get install -y oracle-java7-installer



curl -sL https://deb.nodesource.com/setup | sudo bash - && \
apt-get update && \
apt-get install -y curl wget mc joe git nodejs && \
npm install -g nide && \
wget https://download.jetbrains.com/webstorm/WebStorm-10.0.2.tar.gz && \
cd /tmp && \
wget https://download.jetbrains.com/webstorm/WebStorm-10.0.2.tar.gz && \
tar -zxvf WebStorm-10.0.2.tar.gz && \
mv WebStorm-141.728/ /opt && \
chmod 777 /opt/WebStorm-141.728/bin/webstorm.sh

# /opt/WebStorm-141.728/bin/webstorm.sh
