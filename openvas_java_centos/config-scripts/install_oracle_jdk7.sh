#!/bin/bash

# choose ok on install menu
yum update -y

rpm -ivh /vagrant/jdk-8u40-linux-i586.rpm


cat <<EOT >> /etc/profile.d/java.sh
JAVA_HOME=/usr/java/jdk1.8.0_40/
PATH=$JAVA_HOME/bin:$PATH
export PATH JAVA_HOME
export CLASSPATH=.

EOT

chmod +x /etc/profile.d/java.sh

source /etc/profile.d/java.sh

