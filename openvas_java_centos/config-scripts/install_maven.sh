#!/bin/bash

cd /tmp
wget http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.3.1/binaries/apache-maven-3.3.1-bin.zip

unzip apache-maven-3.3.1-bin.zip

sudo mv apache-maven-3.3.1 /opt/maven

ln -s /opt/maven/bin/mvn /usr/bin/mvn

cat <<EOT >> /etc/profile.d/maven.sh
#!/bin/bash
MAVEN_HOME=/opt/maven
PATH=$MAVEN_HOME/bin:$PATH
export PATH MAVEN_HOME
export CLASSPATH=.

EOT

chmod +x /etc/profile.d/maven.sh

source /etc/profile.d/maven.sh

mvn -version

