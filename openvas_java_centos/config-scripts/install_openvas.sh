#!/bin/bash

apt-get -y install python-software-properties
add-apt-repository "deb http://download.opensuse.org/repositories/security:/OpenVAS:/UNSTABLE:/v5/xUbuntu_12.04/ ./"
apt-key adv --keyserver hkp://keys.gnupg.net --recv-keys BED1E87979EAFD54
apt-get update
apt-get -y --force-yes install greenbone-security-assistant gsd openvas-cli openvas-manager openvas-scanner openvas-administrator sqlite3 xsltproc
