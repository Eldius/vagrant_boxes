#!/bin/bash

cd /tmp
wget http://dl.google.com/android/android-sdk_r24.1.2-linux.tgz
tar -zxvf android-sdk_r24.1.2-linux.tgz
mv ./android-sdk-linux /opt


cp /vagrant/config-scripts/config-files/android.sh /etc/profile.d/

chmod +x /etc/profile.d/android.sh

source /etc/profile.d/android.sh
