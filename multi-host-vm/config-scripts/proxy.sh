#!/bin/bash

echo Installing JDK 8...

# choose ok on install menu
echo debconf shared/accepted-oracle-license-v1-1 select true | \
debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
debconf-set-selections

#install java 8
apt-get update
apt-get install -y python-software-properties
add-apt-repository ppa:webupd8team/java
apt-get update
apt-get install -y oracle-java8-installer

echo JDK 8 server installed.

apt-get install -y unzip

wget http://mirror.predic8.com/membrane/router/membrane-service-proxy-4.0.18.zip

unzip membrane-service-proxy-*.zip

mv membrane-service-proxy-* /opt/membrane-service-proxy



nginx=stable
add-apt-repository ppa:nginx/$nginx
apt-get update 
apt-get install nginx -y


