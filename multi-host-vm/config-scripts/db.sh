#!/bin/bash

echo Installing MariaDB server...

cat << EOF | debconf-set-selections
mysql-server-5.0 mysql-server/root_password password 123Senha
mysql-server-5.0 mysql-server/root_password_again password 123Senha
mysql-server-5.0 mysql-server/root_password seen true
mysql-server-5.0 mysql-server/root_password_again seen true
EOF

sudo apt-get install software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
sudo add-apt-repository 'deb http://mirror.edatel.net.co/mariadb//repo/10.0/ubuntu trusty main'

sudo apt-get update
sudo apt-get install -y mariadb-server

mysql -u root --password=123Senha -t <<STOP
-- This is a comment inside an sql-command-stream.
create database development;
create database lists_database;
show databases;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'
\q
STOP

service mysql restart

echo MariaDB server installed.

